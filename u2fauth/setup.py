from release import setup

setup(
	 name='trac-u2fauth',
	 author='Alexander von Gluck IV',
	 author_email='alex@terarocket.io',
	 description='Trac U2F Validation Server Authentication Plugin',
	 maintainer='Alexander von Gluck IV',
	 maintainer_email='alex@terarocket.io',
	 url='https://gitlab.com/kallisti5/trac-u2fauth',
	 license='BSD 2 clause',
	 install_requires=['requests'],
	 test_suite='test',
	 tests_require=[
		  'httpretty',
	 ],
	 classifiers=[
		  'License :: OSI Approved :: BSD License',
		  'Operating System :: OS Independent',
		  'Development Status :: 4 - Beta',
		  'Intended Audience :: System Administrators',
		  'Topic :: Internet',
		  'Topic :: Security :: Cryptography',
	 ]
)
